#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Mad Clip Transform
# A very odd extension - made by request - for this forum thread
# https://inkscape.org/forums/extensions/rotate-and-move-a-clipped-image-back-to-position/
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex
from inkex import Transform

import uuid, shutil, re

# import warnings
# warnings.filterwarnings("ignore")

def make_temp_folder(self):
    """
    Creates a temp folder to which files can be written \n
    To remove folder at end of script use: \n
    # Cleanup temp folder \n
    if hasattr(self, 'temp_folder'):
        shutil.rmtree(self.temp_folder)

    :return: A temp folder path string
    """
    import tempfile
    temp_folder = tempfile.mkdtemp()
    self.temp_folder = str(temp_folder)
    return temp_folder

def make_temp_file_from_svg_object(self, svg_object):
    import os
    import time
    temp_file_name = str(uuid.uuid4()) + '.svg'
    # temp_file_name = str(time.time()).replace('.', '') + '.svg'
    svg_text = svg_object.tostring().decode('utf-8')
    if hasattr(self, 'inklin_temp_folder'):
        temp_folder = self.inklin_temp_folder
    else:
        temp_folder = make_temp_folder(self)
    temp_file_path = os.path.join(temp_folder, temp_file_name)
    new_file = open(temp_file_path, 'w')
    new_file.write(svg_text)

    return new_file, temp_file_path

def make_temp_file_copy(self, my_file, extension='.svg'):
    """
    :param my_file: file to make temp copy of
    :param extension: file extension to append to new filename
    :return: temp_file_object (open) , temp_file_path
    """
    import sys
    import shutil
    import os
    import uuid
    # Create random uuid - then add extension
    temp_file_name = str(uuid.uuid4()) + str(extension)
    if hasattr(self, 'temp_folder'):
        temp_folder = self.temp_folder
    else:
        temp_folder = make_temp_folder(self)
    temp_file_path = os.path.join(temp_folder, temp_file_name)
    temp_file = shutil.copy(my_file, temp_file_path)
    temp_file_object = open(temp_file)
    temp_file_object.temp_folder = temp_folder
    return temp_file_object, temp_file_path

def inkscape_command_call_process_svg(self, input_file, option_list, action_list):
    """
    A function to execute an Inkscape command call on a temp_file copy of the
    input_file and return the resulting file object. \n
   :param input_file: input file path ( usually self.options.input_file )
   :param option_list: inkscape command line options
   :param action_list: inkscape command line actions ( and verbs in Inskcape 1.1 )
   :return: Returns the resulting svg file object ( which must be .closed() later ) and the temp_file_path
   """
    from inkex import command
    # First make a copy of the input_file
    temp_svg, temp_file_path = make_temp_file_copy(self, input_file)
    import os
    processed_svg_filename = str(uuid.uuid4()) + '.svg'
    processed_svg_filepath = os.path.join(self.temp_folder, processed_svg_filename)
    # Then run the command line

    command.inkscape(temp_svg.name, option_list, f'--actions={action_list};export-filename:{processed_svg_filepath};export-do')
    # inkex.errormsg(temp_svg)
    temp_svg.close()

    processed_svg = open(processed_svg_filepath, 'r')

    return processed_svg, processed_svg_filepath

def get_inverse_transform(self, transform):
    import numpy
    my_object_np_matrix = list(transform.matrix)
    my_object_np_matrix.append((0, 0, 1))

    inverse_matrix = numpy.linalg.inv(my_object_np_matrix)

    inkscape_inverse_matrix = inverse_matrix.tolist()
    inkscape_inverse_matrix.pop(2)

    inverse_transform = Transform().add_matrix(inkscape_inverse_matrix)

    return inverse_transform

class MadClipTransform(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):
        selection_list = self.svg.selected
        if len(selection_list) != 2:
            inkex.errormsg('Please select 2 objects 1 grouped path and 1 image')
            return

        tag_list = [selection_list[0].TAG, selection_list[1].TAG]
        if not 'image' or not 'g' in tag_list:
            inkex.errormsg('Please select 2 objects 1 path and 1 image')
            return

        for item in selection_list:
            if item.TAG == 'g':
                group_transform = item.transform
                inverse_group_transform = get_inverse_transform(self, group_transform)

                group_parent = item.getparent()
                group_children = item.getchildren()
                child = group_children[0]
                child_id = child.get_id()
                group_parent.append(child)
                # Apply group transform to child
                child.transform = child.transform @ group_transform
            if item.TAG == 'image':
                dupe_image = item.duplicate()
                dupe_image_id = dupe_image.get_id()

        # Carry out the clip
        # We are going to operate on a file copy of self.svg, then extract what we need !
        # This results in the an image ( id does not change ) with a clip-path attribute
        # this attribute is stored in <defs> of the svg

        new_file, temp_file_path = make_temp_file_from_svg_object(self, self.svg)

        # Remove the dupe image from the svg
        dupe_image.delete()


        # The action list - deletes everything in the svg apart from the path and the image
        options_list = ''
        action_list = f'select-by-id:{child_id},{dupe_image_id};select-invert;delete;select-all;object-set-clip'

        # The we run the command line and get the processed svg
        processed_svg, processed_svg_filepath = inkscape_command_call_process_svg(self, temp_file_path, options_list, action_list)
        processed_svg_element = inkex.load_svg(processed_svg_filepath).getroot()

        # Get the id of the clipped image - it's the only image in the
        # svg now, id should not change but use xpath to be safe
        clipped_image = processed_svg_element.xpath('//svg:image')[0]

        # Add the clipped image to the current self.svg
        # which is passed back to Inkscape gui
        group_parent.append(clipped_image)

        # Get the clippath from the svg - this is stored as clip-path="url(#pathid)
        # without this the image will be unchanged as the clip-path url will be invalid
        # use regex to get the clip_path id
        clip_path_id = re.findall(r'\(.*?\)', clipped_image.get('clip-path'))[0].replace('(', '').replace(')', '').replace('#', '')
        clip_path = processed_svg_element.getElementById(clip_path_id)
        # Add the clip path of the current defs - xpath always returns a list so append to [0]
        self.svg.xpath('//svg:defs')[0].append(clip_path)

        # Apply the inverse of the orginal group transform to the clipped image
        clipped_image.transform = clipped_image.transform @ inverse_group_transform

        # Close the temp files
        new_file.close()
        processed_svg.close()

        # Cleanup temp folder \n
        if hasattr(self, 'temp_folder'):
            shutil.rmtree(self.temp_folder)
        
if __name__ == '__main__':
    MadClipTransform().run()
